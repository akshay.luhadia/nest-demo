import { Controller, Get, Param, Post, Body } from '@nestjs/common';
import { UsersService, User } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get()
  getUsers() {
    return this.usersService.findAll();
  }

  @Get(':name')
  getOneUser(@Param('name') name: string) {
    return this.usersService.findOne(name);
  }

  @Post()
  createUser(@Body() user: User) {
    return this.usersService.createUser(user.username, user.password);
  }
}
