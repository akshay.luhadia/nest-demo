import { Injectable } from '@nestjs/common';

export type User = any;

@Injectable()
export class UsersService {
  private readonly users: User[];
  constructor() {
    this.users = [
      {
        userId: 1,
        username: 'john',
        password: 'changeme',
      },
      {
        userId: 2,
        username: 'chris',
        password: 'secret',
      },
      {
        userId: 3,
        username: 'maria',
        password: 'guess',
      },
    ];
  }

  async findOne(username: string): Promise<User | undefined> {
    return this.users.find(user => user.username === username);
  }

  async findAll(): Promise<User | undefined> {
    return this.users;
  }

  async createUser(
    username: string,
    password: string,
  ): Promise<User | undefined> {
    let user = {
      userId: this.users.length + 1,
      username,
      password,
    };

    this.users.push(user);
    return { userId: user.userId, username };
  }
}
